export const environment = {
  production: true,
  url: 'https://integrated-ticket-support-api.herokuapp.com',
  analytics: 'https://ticket-submisson.herokuapp.com/analytics'
};
