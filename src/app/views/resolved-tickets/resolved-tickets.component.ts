import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TicketHubService } from '../../services/ticket-hub.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-resolved-tickets',
  templateUrl: './resolved-tickets.component.html',
  styleUrls: ['./resolved-tickets.component.css']
})
export class ResolvedTicketsComponent implements OnInit {
  resolvedTicketModal: FormGroup;
  resolvedTicketList: any = {};
  data: any = {};
  groupID: any;
  agent_id: any;
  GroupName: any;
  modalRef: BsModalRef;
  ticketIDPre: string = 'CMS/TIC/00';

  constructor(private formBuilder: FormBuilder, private modalService: BsModalService,
    private api: TicketHubService, private toastr: ToastrService) {
      this.groupID = sessionStorage.getItem('GroupID');
      this.agent_id = sessionStorage.getItem('agent_id');
    }

  ngOnInit(): void {
    this.loadResolvedTickets();
  }

  loadResolvedTickets() {
    this.resolvedTicketList = {};

    const requestParams = {
      groupID: this.groupID,
      agent_id: this.agent_id
    };

    this.api.getResolvedByGroup(requestParams).subscribe(response => {
      console.log(response);
      if (response.status === 200) {
        this.data = response;
        console.log(response.status);
        this.resolvedTicketList = this.data.data;
        this.GroupName = this.data.data[0].GroupName;
      } else {
        console.log('no data');
        this.toastr.error('No Data Found', 'CMS Support Hub');
      }
    });
  }

  openResolvedTicketFormModal(d, template: TemplateRef<any>) {
    this.resolvedTicketModal = this.formBuilder.group({
      id: this.ticketIDPre + [d.id],
      ticketID: [d.ticketID],
      subject: [d.subject],
      actionPerformed: [d.ActionPerformed],
      date: [d.resolvedOn],
      resolvedBy: [d.SupportStaff],
    });
    this.modalRef = this.modalService.show(template);
  }

}
