import {Component, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TicketHubService} from '../../services/ticket-hub.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-unresolved-tickets',
  templateUrl: './unresolved-tickets.component.html',
  styleUrls: ['./unresolved-tickets.component.css']
})
export class UnresolvedTicketsComponent implements OnInit {
  unresolvedTicketModalForm: FormGroup;
  unresolvedTicketsList: any = {};
  platforms: FormGroup;
  platformList: any = {};
  data: any = {};
  modalRef: BsModalRef;
  pageNum: number;
  groupID: any;
  agent_id: any;
  ticketIDPre: string = 'CMS/TIC/00';
  GroupName: any;

  constructor(private formBuilder: FormBuilder, viewRefContainer: ViewContainerRef,
              private modalService: BsModalService, private api: TicketHubService,
              public toaster: ToastrService) {
                this.groupID = sessionStorage.getItem('GroupID');
                this.agent_id = sessionStorage.getItem('agent_id');
              }

  ngOnInit(): void {
    this.loadUnresolvedTicketsByGroup();
    // this.loadTickets();
    // this.loadPlatforms();
  }

  // createPlatformSelect() {
  //   this.platforms = this.formBuilder.group({
  //     platform: ['', Validators.required]
  //   });
  // }

  loadUnresolvedTicketsByGroup() {
    this.unresolvedTicketsList = {};

    const requestParams = {
      groupID: this.groupID,
      agent_id: this.agent_id
    };

    this.api.getInvalidByGroup(requestParams).subscribe(response => {
      console.log(response.status);
      if (response.status === 404) {
        this.toaster.error('No Data', 'CMS Support Hub');
      } else {
        this.data = response;
        console.log(this.data);
        this.unresolvedTicketsList = this.data.data;
        this.GroupName = this.data.data[0].GroupName;
      }
    });
  }

  // loadTickets() {
  //   this.loadTicketsByPlatform();
  // }

  // loadPlatforms() {
  //   this.platformList = {};
  //   this.api.getAllPlatforms().subscribe(response => {
  //     this.data = response;
  //     console.log(this.data);
  //     this.platformList = this.data.data;
  //   });
  // }

  openUnresolvedTicketFormModal(d, template: TemplateRef<any>) {
    this.unresolvedTicketModalForm = this.formBuilder.group({
      id: [d.ticketID],
      ticketID: [d.ticketID],
      subject: [d.subject],
      createdOn: [d.createdOn],
      firstName: [d.firstName],
      email: [d.email],
      ActionPerformed: [d.ActionPerformed],
    });
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-dialog modal-dialog-centered'
    });
  }
}
