import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, NgModel} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import { ConfirmedValidator } from '../../confirmed.validator';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  RegistrationForm: FormGroup;
  passwordIsValid = false;
  jsonBucket: any = {};
  password: NgModel;
  confirmPassword: NgModel;
  toolTipMsg = `Password must have:
  An Upper Case letter,
  A Special Character,
  A Number, and
  A Lower Case Letter`;
  constructor(private formBuilder: FormBuilder, private router: Router, private api: AuthService, public toastr: ToastrService) {
    // this.createRegistrationForm();
    this.createRegistrationForm();
  }

  ngOnInit(): void {}

  createRegistrationForm() {
    this.RegistrationForm = this.formBuilder.group({
      // agent_id: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
    }, {validator: ConfirmedValidator('password', 'confirmPassword')});
  }
  get f() {
    return this.RegistrationForm.controls;
  }
  register() {
    console.log(this.RegistrationForm.value);
    this.api.register(this.RegistrationForm.value).subscribe(response => {
      console.log(response);
      this.jsonBucket = response;

      if (this.jsonBucket.status === 400) {
        console.log(this.jsonBucket.status);
        this.toastr.error('Oops! An Error Occured', 'CMS Support Hub');
      } else {
        this.toastr.success('User Profile Created', 'CMS Support Hub');
        this.router.navigate(['login']);
      }
    });
  }

  passwordValid(event) {
    this.passwordIsValid = event;
  }
}
