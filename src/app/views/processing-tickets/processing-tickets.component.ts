import {Component, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TicketHubService} from '../../services/ticket-hub.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-processing-tickets',
  templateUrl: './processing-tickets.component.html',
  styleUrls: ['./processing-tickets.component.css']
})
export class ProcessingTicketsComponent implements OnInit {
  processingTicketList: any = {};
  ticketInformationForm: FormGroup;
  data: any = {};
  resolvedBy: any;
  processingTicketFormModal: FormGroup;
  oneTimePassword: FormGroup;
  modalRef: BsModalRef;
  pageNum: number;
  status = 'resolved';
  ticketIDPre: string = 'CMS/TIC/00';
  groupID: any;
  agent_id: any;
  id: any;
  GroupName: any;
  userName: any;
  email: string;
  phone: string;
  createdOn: string;
  firstName: string;
  issue: string;
  subject: string;
  platform: string;
  platformID: string;
  TicketStatus: string;
  ticketID: string;
  d: any;

  constructor(private formBuilder: FormBuilder, viewRefContainer: ViewContainerRef,
              private modalService: BsModalService, private api: TicketHubService,
              private toastr: ToastrService) {
                this.groupID = sessionStorage.getItem('GroupID');
                this.userName = sessionStorage.getItem('userName');
                this.agent_id = sessionStorage.getItem('agent_id');
                this.createTicketInformationForm();
              }

  ngOnInit(): void {
    this.LoadProcessingTickets();
    console.log(this.userName);
    console.log(this.groupID);
    console.log(typeof(this.groupID));
  }

  // @doc::: LoadProcessingTickets will obtain all tickets of status 'processing' from the API belonging to a specific
  // support group using the set session groupID
  LoadProcessingTickets() {
    this.processingTicketList = {};

    const requestParams = {
      groupID: this.groupID,
      agent_id: this.agent_id
    };

    this.api.getProcessingByGroup(requestParams).subscribe(response => {
      this.data = response;
      console.log(this.data);
      this.processingTicketList = this.data.data;
      this.GroupName = this.data.data[0].GroupName;
    });
  }



  // @doc::: Deprecated Method
  openMarkTicketResolvedFormModal(template: TemplateRef<any>) {
    this.d = this.ticketInformationForm.value;
    console.log(this.d);
    this.processingTicketFormModal = this.formBuilder.group({
      id: [this.d.id],
      ticketID: [this.d.ticketID],
      subject: [this.d.subject],
      issue: [this.d.issue],
      createdOn: [this.d.createdOn],
      firstName: [this.d.firstName],
      uploadedFile: [this.d.uploadedFile],
      actionPerformed: ['', Validators.required],
      email: [this.d.email],
      secureKey: ['', Validators.required],
      status: this.status,
      action: 'Resolve',
      resolvedBy: this.userName
    });
    this.modalRef = this.modalService.show(template);
  }

  // @doc::: openOneTimePasswordFormModal will open the one time password needed for closing a ticket
  openOneTimePasswordFormModal(template: TemplateRef<any>) {
    this.oneTimePassword = this.formBuilder.group({
      oneTimePassword: ['', Validators.required]
    });
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-dialog modal-dialog-centered',
      ignoreBackdropClick: false,
      backdrop: true
    });
  }

  // @doc::: resolveTicket will append the support staff password to the ticket information
  // form and submit to MarkTicketAsResolved service
  resolveTicket() {
    this.ticketInformationForm.patchValue({
      secureKey: this.oneTimePassword.value['oneTimePassword']
    });
    console.log(this.ticketInformationForm.value);

    this.api.MarkTicketAsResolved(this.ticketInformationForm.value).subscribe(response => {
      if (response.status === 200) {
        this.toastr.success('Ticket Updated', 'CMS Support Hub');
        this.ticketInformationForm.reset();
        this.modalRef.hide();
        this.ngOnInit();
      } else {
        this.toastr.success('Ticket Updated', 'CMS Support Hub');
      }
    });
  }

  // @doc::: getTicketInfo takes a ticket instance and
  // poulates the ticket information form,
  getTicketInfo(data: any) {
    console.log(data);
    this.email = data.email;
    this.phone = data.phone;
    this.createdOn = data.createdOn;
    this.issue = data.issue;
    this.firstName = data.firstName;
    this.subject = data.subject;
    this.ticketID = data.ticketID;
    this.platform = data.platform;
    this.platformID = data.platformID;
    this.TicketStatus = data.status;
    this.id = data.id;
    console.log(data.status);

    this.ticketInformationForm.patchValue({
      createdOn: data.createdOn,
      firstName: data.firstName,
      ticketID: data.ticketID,
      ticketStatus: data.status,
      email: data.email,
      platform: data.platform,
      subject: data.subject,
      issue: data.issue,
      id: parseInt(this.id, 10),
      groupID: parseInt(this.groupID, 10)
    });
    // a test::: console.log(this.ticketInformationForm.value);
  }

  createTicketInformationForm() {
    this.ticketInformationForm = this.formBuilder.group({
      createdOn: ['', Validators.required],
      firstName: ['', Validators.required],
      ticketStatus: ['', Validators.required],
      email: ['', Validators.required],
      subject: ['', Validators.required],
      issue: ['', Validators.required],
      ticketID: ['', Validators.required],
      platform: ['', Validators.required],
      actionPerformed: ['', Validators.required],
      secureKey: ['', Validators.required],
      action: 'Resolve',
      resolvedBy: this.userName,
      status: this.status,
      groupID: ['', Validators.required],
      id: ['', Validators.required]
    });
  }

  // found a way to pass ticket data the form
  displayForm(aForm: any) {
    const display = this.openOneTimePasswordFormModal(aForm);
    console.log(display);
  }
}


/*
bid_amount: null - 1
condition: "New" - 1
created_by: 1 - 1
end_date: null
id: 2 - 1
image: "C:\fakepath\register.JPG" - 1
item_name: "2019 Honda Civic" - 1
quantity: 1 - 1
start_date: null
status: "open" - 1
title: "Automobile Sale" - 1
win_bid: 123 - 1
 */
