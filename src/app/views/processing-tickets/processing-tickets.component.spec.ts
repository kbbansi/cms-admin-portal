import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessingTicketsComponent } from './processing-tickets.component';

describe('ProcessingTicketsComponent', () => {
  let component: ProcessingTicketsComponent;
  let fixture: ComponentFixture<ProcessingTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessingTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessingTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
