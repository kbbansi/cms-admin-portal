import {Component, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TicketHubService} from '../../services/ticket-hub.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-new-tickets',
  templateUrl: './new-tickets.component.html',
  styleUrls: ['./new-tickets.component.css']
})
export class NewTicketsComponent implements OnInit {
  ticketModal: FormGroup;
  platforms: FormGroup;
  ticketSupportMail: FormGroup;
  ticketList: any = {};
  platformList: any = {};
  data: any = {};
  pageNum: number;
  From: string = 'digitalinnovagh@gmail.com';
  subject: string = 'Support Request for Ticket ID :- ';
  issue: string;
  upload: string;
  resolvedBy: any;
  status: string = 'resolved';
  ticketIDPre: string = 'CMS/TIC/00';
  modalRef: BsModalRef;
  supportGroup: any;
  oneTimePassword: FormGroup;
  groupID: any;
  agent_id: any;
  GroupName: any;
  userName: string;
  constructor(private formBuilder: FormBuilder, viewRefContainer: ViewContainerRef, private modalService: BsModalService,
              private api: TicketHubService, public toastr: ToastrService) {
                this.groupID = sessionStorage.getItem('GroupID');
                this.userName = sessionStorage.getItem('userName');
                this.agent_id = sessionStorage.getItem('agent_id');
              }

  ngOnInit() {
    this.loadTicketByGroup();
  }

  // create platforms form // not needed
  // createTicketPlatformFormGroup() {
  //   this.platforms = this.formBuilder.group({
  //     platform: ['', Validators.required]
  //   });
  // }

  // this will be come loadTicketByGroup(d)
  loadTicketByGroup() {
    console.log(this.groupID);
    console.log(this.agent_id);
    const requestParams = {
      groupID: this.groupID,
      agent_id: this.agent_id
    };

    this.ticketList = {};
   // console.log(this.platforms.value['platform']);
    this.api.getTicketByGroup(requestParams).subscribe(response => {
      console.log(response);
      if (response.status !== 200) {
        this.toastr.error('No Ticket Found', 'CMS Support Hub');
      } else {
        this.data = response;
        this.ticketList = this.data.data;
        this.GroupName = this.data.data[0].GroupName;
      }
    });
  }


  // loadTicketByPlatform() {
  //   this.ticketList = {};
  //   console.log(this.platforms.value['platform']);
  //   this.api.getTicketByPlatform(this.platforms.value['platform']).subscribe(response => {
  //     if (response['status'] !== 'Ok') {
  //       this.toastr.error('No Ticket Found', 'CMS Support Hub');
  //     } else {
  //       this.data = response;
  //       console.log(this.data);
  //       this.ticketList = this.data.data;
  //     }
  //   });
  // }

  // // not needed
  // loadPlatforms() {
  //   this.platformList = {};
  //   this.api.getAllPlatforms().subscribe(response => {
  //     this.data = response;
  //     console.log(this.data);
  //     this.platformList = this.data.data;
  //   });
  // }

  // // not needed
  // loadTickets() {
  //   this.loadTicketByPlatform();
  // }

  // invalid form
  openTicketResponseFormModal(d, template: TemplateRef<any>) {
    this.ticketModal = this.formBuilder.group({
      id: [d.id],
      ticketID: [d.ticketID],
      subject: [d.subject],
      issue: [d.issue],
      createdOn: [d.createdOn],
      firstName: [d.firstName],
      uploadedFile: [d.linkToFile],
      email: [d.email],
      actionPerformed: ['', Validators.required],
      secureKey: ['', Validators.required],
      status: 'invalid',
      action: 'Invalid',
      resolvedBy: this.userName,
      groupID: [d.groupID],
      platform: [d.platform]
    });
    this.modalRef = this.modalService.show(template);
  }

  openOneTimePasswordFormModal(template: TemplateRef<any>) {
    this.oneTimePassword = this.formBuilder.group({
      oneTimePassword: ['', Validators.required]
    });
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-dialog modal-dialog-centered',
      ignoreBackdropClick: false,
      backdrop: true
    });
  }

  openTicketMailerFormModal(d, template: TemplateRef<any>) {
    this.ticketSupportMail = this.formBuilder.group({
      id: [d.id],
      To: [d.email],
      From: this.From,
      Subject: [d.subject],
      Message: ['', Validators.required],
      issue: [d.issue],
      TicketID: [d.ticketID],
      createdOn: [d.createdOn],
      firstName: [d.firstName],
      uploadedFile: [d.linkToFile],
      status: ['', Validators.required]
    });
    this.upload = this.ticketSupportMail.value.uploadedFile;
    console.log(this.upload);
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-dialog modal-dialog-centered'
    });
  }

  // todo:: build complete resovlveTicket() function

  // mark invalid ticket
  markTicketInvalid() {
    // this.toastr.success('Hi', 'CMS Support Hub');
    this.ticketModal.patchValue({
      secureKey: this.oneTimePassword.value['oneTimePassword']
    });
    console.log(this.ticketModal.value);

   this.api.MarkTicketAsResolved(this.ticketModal.value).subscribe(response => {
     if (response.status === 200) {
       this.toastr.info('Ticket Marked as Invalid', 'CMS Support Hub');
       this.ticketModal.reset();
       this.modalRef.hide();
       this.modalRef.hide();
       this.ngOnInit();
     } else if (response.status === 400 || response.status === 500) {
      this.toastr.warning('An Error Occured', 'CMS Support Hub');
     }
   });
  }

  viewFile() {
    console.log(this.ticketSupportMail.value.uploadedFile);
    window.open(this.ticketSupportMail.value.uploadedFile);
  }

  sendMail() {
    this.ticketSupportMail.patchValue({
      status: 'processing'
    });
    console.log(this.ticketSupportMail.value);
    this.api.SendMail(this.ticketSupportMail.value).subscribe(response => {
      console.log(response);
      if (response.status === 200) {
        this.toastr.success('Email Sent', 'CMS Support Hub');
        this.ticketSupportMail.reset();
        this.ngOnInit();
        this.modalRef.hide();
      } else if (response.status === 400) {
        this.toastr.error('Email Not Sent', 'CMS Support Hub');
        this.ticketSupportMail.reset();
        this.ngOnInit();
        this.modalRef.hide();
      }
    });
  }
}
