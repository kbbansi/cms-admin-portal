import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { TicketHubService } from '../../services/ticket-hub.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  userFirstName: string;
  userLastName: string;
  userGroupID: string;
  jsonBucket: any = {};
  constructor(private formBuilder: FormBuilder, private router: Router, private api: AuthService) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm() {
    this.LoginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  submit() {
    this.jsonBucket = {};
    console.log(this.LoginForm.value);
    this.api.login(this.LoginForm.value).subscribe(response => {
      // set session variables here
      console.log(response);
      this.jsonBucket = response.data;
      console.log(this.jsonBucket[0].firstName);
      this.setSessionVariables(this.jsonBucket);
      this.router.navigate(['dashboard']);
      console.log(sessionStorage.getItem('GroupID'));
    });
  } // [""0""].firstName

  setSessionVariables(d) {
    sessionStorage.setItem('GroupID', d[0].groupID);
    sessionStorage.setItem('email', d[0].email);
    sessionStorage.setItem('userName', d[0].userName);
    sessionStorage.setItem('agent_id', d[0].agent_id);
    console.log(sessionStorage.getItem('GroupID'));
  }
}
/*
id: 1
firstName: "Kwabena Amo"
lastName: "Ampofo"
email: "kwabenaampofo5@gmail.com"
userName: "kwabena.ampofo"
password: "$2b$04$0cyeYwumjJtDLUn0gL8KnOHnv4AIQLiyXI0DpTfvr/bryjNjnjx7u"
groupID: "1"

*/
