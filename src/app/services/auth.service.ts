import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  api = environment.url;
  jsonBucket: any = {};

  constructor(private https: HttpClient, private router: Router) { }

  login(d) {
    console.log(d);

    return this.https.post(this.api + '/cms-login/auth', d)
      .pipe(map((response: any) => response));
  }

  register(d) {
    console.log(d);
    return this.https.put(this.api + '/cms-support/update-profile', d)
      .pipe(map((response: any) => response));
  }


}
/**
 *
    return this.https.post(this.api + '/cms-login/auth', d)
    .pipe(map((response: any) => response));
 *
 */
