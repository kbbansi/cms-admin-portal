import { TestBed } from '@angular/core/testing';

import { TicketHubService } from './ticket-hub.service';

describe('TicketHubService', () => {
  let service: TicketHubService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketHubService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
