import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TicketHubService {
  api = environment.url;
  analytics = environment.analytics;
  cms = {
    cms: '/cms-support-ticket',
    chart: '/cms_subject',
    newTickets: '/cms_ticket_new',
    processing: '/cms_ticket_processing',
    resolved: '/cms_ticket_resolved',
    unresolved: '/cms_ticket_unresolved',
    tickets: '/cms_tickets',
    platforms: '/cms_platform',
    platform: '/cms-support-platform',
    highestPlatform: '/cms_highest_platform',
    mail: '/send-mail',
    newTicket: '/new/',
    process: '/processing_ticket',
    resolvedTicket: '/resolved_ticket',
    unresolvedTicket: '/unresolved_ticket',
    unresolvedTicketPlatform: '/unresolved/'
  };
  constructor(private http: HttpClient) { }

  // this will return all tickets created
  getTickets() {
    return this.http.get(this.analytics + this.cms.tickets)
      .pipe(map((response: any) => response));
  }

  getProcessing() {
    return this.http.get(this.analytics + this.cms.processing)
      .pipe(map((response: any) => response));
  }

  getPlatforms() {
    return this.http.get(this.analytics + this.cms.platforms)
      .pipe(map((response: any) => response));
  }

  getHighestPlatform() {
    return this.http.get(this.analytics + this.cms.highestPlatform)
      .pipe(map((response: any) => response));
  }

  getTicketsNew() {
    return this.http.get(this.analytics + this.cms.newTickets)
      .pipe(map((response: any) => response));
  }

  getGraph() {
    return this.http.get(this.analytics + this.cms.chart)
      .pipe(map((response: any) => response));
  }

  getTicketByPlatform(d) {
    console.log(d);
    return this.http.get(this.api + this.cms.cms + this.cms.newTicket + d)
      .pipe(map((response: any) => response));
  }

  getTicketByGroup(d) {
    console.log(d);
    return this.http.get(this.api + '/cms-support/ticket-group/' + d.groupID + '/' + d.agent_id)
      .pipe(map((response: any) => response));
  }

  getAllPlatforms() {
    return this.http.get(this.api + this.cms.platform)
      .pipe(map((response: any) => response));
  }

  SendMail(d) {
    console.log(d);
    return this.http.post(this.api + '/cms-support-ticket/send-mail', d)
      .pipe(map((response: any) => response));
  }

  getProcessingTickets() {
    return this.http.get(this.api + '/cms-support-ticket/processing_ticket')
      .pipe(map((response: any) => response));
  }

  getResolvedTicketList() {
    console.log('hello world');
    return this.http.get(this.api + '/cms-support-ticket/resolved_ticket')
      .pipe(map((response: any) => response));
  }

  getUnresolvedByPlatform(d) {
    console.log('hello world unresolved');
    return this.http.get(this.api + this.cms.cms + this.cms.unresolvedTicketPlatform + d)
      .pipe(map((response: any) => response));
  }

  getProcessingByGroup(d) {
    // console.log(d);
    return this.http.get(this.api + '/cms-support/ticket-group-processing/' + d.groupID + '/' + d.agent_id)
      .pipe(map((response: any) => response));
  }

  getResolvedByGroup(d) {
    // console.log(d);
    return this.http.get(this.api + '/cms-support/ticket-group-resolved/' + d.groupID + '/' + d.agent_id)
      .pipe(map((response: any) => response));
  }

  getAllInvalidTickets() {
    // console.log(d);
    return this.http.get(this.api + '/cms-support/ticket-group-invalid/all')
      .pipe(map((response: any) => response));
  }

  getInvalidByGroup(d) {
    // console.log(d);
    return this.http.get(this.api + '/cms-support/ticket-group-invalid/' + d.groupID + '/' + d.agent_id)
      .pipe(map((response: any) => response));
  }

  MarkTicketAsResolved(d) {
    console.log(d);
    return this.http.put(this.api + this.cms.cms + '/update/' + d.id, d)
      .pipe(map((response: any) => response));
  }
}
