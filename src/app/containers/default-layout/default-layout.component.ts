import {Component } from '@angular/core';
import { navItems } from '../../_nav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  agent_id: any;
  public sidebarMinimized = false;
  public navItems = navItems;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  constructor(private router: Router) {
    this.agent_id = sessionStorage.getItem('agent_id');
    console.log(this.agent_id);
  }

  logOut() {
    sessionStorage.clear();
    this.router.navigateByUrl('login').then(r => {
      console.log(r);
    });
  }
}
